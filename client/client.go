package client

import (
	"context"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/tools/yarascan"
)

type Client struct {
	be clientutil.Backend
}

func New(config *clientutil.BackendConfig) (*Client, error) {
	be, err := clientutil.NewBackend(config)
	if err != nil {
		return nil, err
	}
	return &Client{be}, nil
}

func (c *Client) Submit(ctx context.Context, dd []*yarascan.Detection) error {
	req := yarascan.SubmissionRequest{
		Detections: dd,
	}
	return c.be.Call(ctx, "", "/api/submission", &req, nil)
}

func (c *Client) FindDetectionsBySite(ctx context.Context, site string) ([]*yarascan.PathDetection, error) {
	var resp yarascan.FindDetectionsBySiteResponse
	if err := c.be.Call(ctx, "", "/api/search/by_site", &yarascan.FindDetectionsBySiteRequest{
		Site: site,
	}, &resp); err != nil {
		return nil, err
	}
	return resp.Detections, nil
}

func (c *Client) GetActiveDetectionPaths(ctx context.Context, host string) ([]string, error) {
	var resp yarascan.FindUnresolvedDetectionPathsResponse
	if err := c.be.Call(ctx, "", "/api/search/unresolved", &yarascan.FindUnresolvedDetectionPathsRequest{
		Host: host,
	}, &resp); err != nil {
		return nil, err
	}
	return resp.Paths, nil
}

func (c *Client) ResolveDetections(ctx context.Context, host string, entries []*yarascan.ResolveEntry) error {
	return c.be.Call(ctx, "", "/api/resolve", &yarascan.ResolveRequest{
		Host:    host,
		Entries: entries,
	}, nil)
}
