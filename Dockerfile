FROM debian:bookworm-slim AS build
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends build-essential libyara-dev pkg-config golang-go zlib1g-dev libbz2-dev liblzma-dev

ADD . /src
WORKDIR /src
RUN env CGO_LDFLAGS="-ljansson -lmagic -lz -lbz2 -llzma -lssl -lcrypto -lm" \
    go build -trimpath -ldflags='-extldflags=-static -w -s' -tags osusergo,netgo,sqlite_omit_load_extension -o /yarascan ./cmd/yarascan

FROM scratch
COPY --from=build /yarascan /usr/bin/yarascan

ENTRYPOINT ["/usr/bin/yarascan"]
