package main

import (
	"context"
	"flag"
	"log"
	"os"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/tools/yarascan"
	"git.autistici.org/ai3/tools/yarascan/client"
	"github.com/google/subcommands"
)

type checkCommand struct {
	url     string
	tlsCert string
	tlsKey  string
	tlsCA   string
}

func (c *checkCommand) Name() string { return "check" }
func (c *checkCommand) Synopsis() string {
	return "check active detections against the local filesystem"
}
func (c *checkCommand) Usage() string {
	return `check [<flags>]:
	Check the active detections known to a remote server against the local
	filesystem, and eventually resolve them.

`
}

func (c *checkCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.url, "server", "", "check server `URL`")
	f.StringVar(&c.tlsCert, "tls-cert", "", "TLS certificate `path`")
	f.StringVar(&c.tlsKey, "tls-key", "", "TLS private key `path`")
	f.StringVar(&c.tlsCA, "tls-ca", "", "TLS CA `path`")
}

func (c *checkCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	remote, err := client.New(c.backendConfig())
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	tctx, cancel := context.WithTimeout(ctx, 300*time.Second)
	active, err := remote.GetActiveDetectionPaths(tctx, yarascan.Hostname)
	cancel()
	if err != nil {
		log.Printf("error fetching active detection paths: %v", err)
		return subcommands.ExitFailure
	}
	log.Printf("found %d active detections on this host", len(active))

	// For each path, check if it still exists.
	var gone []string
	for _, path := range active {
		if _, err := os.Stat(path); os.IsNotExist(err) {
			gone = append(gone, path)
		}
	}

	if len(gone) > 0 {
		log.Printf("resolving %d detections whose source files are gone", len(gone))
		var entries []*yarascan.ResolveEntry
		for _, path := range gone {
			entries = append(entries, &yarascan.ResolveEntry{Path: path})
		}
		tctx, cancel = context.WithTimeout(ctx, 300*time.Second)
		err = remote.ResolveDetections(tctx, yarascan.Hostname, entries)
		cancel()
		if err != nil {
			log.Printf("error resolving detections: %v", err)
			return subcommands.ExitFailure
		}
	}

	return subcommands.ExitSuccess
}

func (c *checkCommand) backendConfig() *clientutil.BackendConfig {
	config := &clientutil.BackendConfig{
		URL: c.url,
	}
	if c.tlsCert != "" && c.tlsKey != "" && c.tlsCA != "" {
		config.TLSConfig = &clientutil.TLSClientConfig{
			Cert: c.tlsCert,
			Key:  c.tlsKey,
			CA:   c.tlsCA,
		}
	}
	return config
}

func init() {
	subcommands.Register(&checkCommand{}, "")
}
