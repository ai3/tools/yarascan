module git.autistici.org/ai3/tools/yarascan

go 1.14

require (
	git.autistici.org/ai3/go-common v0.0.0-20230816213645-b3aa3fb514d6
	github.com/elazarl/go-bindata-assetfs v1.0.1
	github.com/google/subcommands v1.2.0
	github.com/hillu/go-yara/v4 v4.2.4
	gopkg.in/yaml.v3 v3.0.1
)
