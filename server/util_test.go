package server

import "testing"

func TestHomedirMap(t *testing.T) {
	m := homedirMap(map[string]string{
		"/var/www/site1":       "site1",
		"/var/www/site2":       "site2",
		"/var/www/site2/site3": "site3",
	})

	for _, td := range []struct {
		path, expectedSite string
		expectedOk         bool
	}{
		{"", "", false},
		{"/not/in/scope", "", false},
		{"/var/www/site1", "site1", true},
		{"/var/www/site1/", "site1", true},
		{"/var/www/site1/index.html", "site1", true},
		{"/var/www/site1/ends-with-slash/", "site1", true},
		{"/var/www/site1/deeply/nested/directory/with/a/long/path/index.html", "site1", true},
		{"/var/www/site2/index.html", "site2", true},
		{"/var/www/site2/site3/index.html", "site3", true},
	} {
		site, ok := m.Find(td.path)
		if ok != td.expectedOk || site != td.expectedSite {
			t.Errorf(
				"unexpected result for %s: got site=%s,ok=%v expected site=%s,ok=%v",
				td.path, site, ok, td.expectedSite, td.expectedOk,
			)
		}
	}
}
