package server

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http/httptest"
	"os"
	"path/filepath"
	"testing"
	"time"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/tools/yarascan"
	"git.autistici.org/ai3/tools/yarascan/client"
)

func testDetections() []*yarascan.Detection {
	now := time.Now()
	dd := []*yarascan.Detection{
		&yarascan.Detection{
			Host:      "host1",
			Path:      "/var/www/site1/malware.php",
			Signature: "malware1",
			Timestamp: now,
		},
		&yarascan.Detection{
			Host:      "host1",
			Path:      "/var/www/site1/malware.php",
			Signature: "malware1b",
			Timestamp: now,
		},
		&yarascan.Detection{
			Host:      "host1",
			Path:      "/var/www/site1/more-malware.php",
			Signature: "malware2",
			Timestamp: now,
		},
		&yarascan.Detection{
			Host:      "host2",
			Path:      "/var/www/site2/malware.php",
			Signature: "malware1",
			Timestamp: now,
		},
	}
	return dd
}

func writeSiteInfo(t *testing.T, path string, homedirMap map[string]string) {
	f, err := os.Create(path)
	if err != nil {
		t.Fatalf("%s: %v", path, err)
	}
	defer f.Close()

	var sites []*siteInfo
	for docroot, site := range homedirMap {
		sites = append(sites, &siteInfo{
			Docroot: docroot,
			Site:    site,
		})
	}
	if err := json.NewEncoder(f).Encode(sites); err != nil {
		t.Fatal(err)
	}
}

func startTestServer(t *testing.T, homedirMap map[string]string) (string, *Server, func()) {
	// Start up the server with a temporary database.
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}

	database, err := OpenDB(filepath.Join(dir, "test.db"))
	if err != nil {
		t.Fatal(err)
	}

	var siteInfoFile string
	if homedirMap != nil {
		siteInfoFile = filepath.Join(dir, "sites.json")
		writeSiteInfo(t, siteInfoFile, homedirMap)
	}

	srv, err := New(database, &Config{SiteInfoPath: siteInfoFile})
	if err != nil {
		t.Fatal(err)
	}

	httpSrv := httptest.NewServer(srv.Handler())

	return httpSrv.URL, srv, func() {
		httpSrv.Close()
		srv.Close()
		database.Close()
		os.RemoveAll(dir)
	}
}

func TestServer_Submission(t *testing.T) {
	srvURL, _, cleanup := startTestServer(t, map[string]string{
		"/var/www/site1": "site1",
		"/var/www/site2": "site2",
	})
	defer cleanup()

	// Create a test client and try to insert a few detections.
	c, err := client.New(&clientutil.BackendConfig{
		URL: srvURL,
	})
	if err != nil {
		t.Fatal(err)
	}

	err = c.Submit(context.Background(), testDetections())
	if err != nil {
		t.Fatal(err)
	}

	// Now let's try a query for site1.
	results, err := c.FindDetectionsBySite(context.Background(), "site1")
	if err != nil {
		t.Fatal(err)
	}
	if n := len(results); n != 2 {
		t.Errorf("expecting 2 results, got %d", n)
	}
	// Verify that we have results both for malware.php and more-malware.php.
	for _, r := range results {
		switch r.Path {
		case "/var/www/site1/malware.php":
			if n := len(r.Signatures); n != 2 {
				t.Errorf("expecting 2 signatures for %s, got %d", r.Path, n)
			}
		case "/var/www/site1/more-malware.php":
		default:
			t.Errorf("unexpected path %s", r.Path)
		}
	}
}

func TestServer_Notify(t *testing.T) {
	srvURL, srv, cleanup := startTestServer(t, map[string]string{
		"/var/www/site1": "site1",
		"/var/www/site2": "site2",
	})
	defer cleanup()

	// Create a test client and try to insert a few detections.
	c, err := client.New(&clientutil.BackendConfig{
		URL: srvURL,
	})
	if err != nil {
		t.Fatal(err)
	}

	err = c.Submit(context.Background(), testDetections())
	if err != nil {
		t.Fatal(err)
	}

	// Test the notification. The second run should be empty.
	n, err := srv.notifyAllPending(context.Background())
	if err != nil {
		t.Fatalf("notifyAllPending/1: %v", err)
	}
	if n != 2 {
		t.Fatalf("notifyAllPending/1: notified %d items, expected 2", n)
	}

	n, err = srv.notifyAllPending(context.Background())
	if err != nil {
		t.Fatalf("notifyAllPending/2: %v", err)
	}
	if n != 0 {
		t.Fatalf("notifyAllPending/2: notified %d items, expected 0", n)
	}
}

func TestServer_ActiveDetections(t *testing.T) {
	srvURL, _, cleanup := startTestServer(t, map[string]string{
		"/var/www/site1": "site1",
		"/var/www/site2": "site2",
	})
	defer cleanup()

	// Create a test client and try to insert a few detections.
	c, err := client.New(&clientutil.BackendConfig{
		URL: srvURL,
	})
	if err != nil {
		t.Fatal(err)
	}

	err = c.Submit(context.Background(), testDetections())
	if err != nil {
		t.Fatal(err)
	}

	paths, err := c.GetActiveDetectionPaths(context.Background(), "host1")
	if err != nil {
		t.Fatalf("GetActiveDetectionPaths: %v", err)
	}

	if len(paths) != 3 {
		t.Fatalf("expected 3 paths, got %v", paths)
	}
}
