package server

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strings"
)

type siteInfo struct {
	Site    string `json:"site"`
	Docroot string `json:"docroot"`
	Owner   string `json:"owner"`
	Status  string `json:"status"`
}

func (i *siteInfo) isActive() bool {
	return i.Status == "active" || i.Status == "readonly"
}

// The homedirMap maps home directories to their associated web
// sites. Site identifiers are opaque strings and can be anything.
// Note that a site can't have the root (/) as a home directory.
type homedirMap map[string]string

// Find the site whose home directory contains path.
func (m homedirMap) Find(path string) (site string, ok bool) {
	for path != "" {
		site, ok = m[path]
		if ok {
			break
		}

		// We use filepath.Split instead of filepath.Dir
		// because it has simpler semantics on the returned
		// dir value and it's easier to check when we're done.
		path, _ = filepath.Split(path)
		path = strings.TrimRight(path, "/")
	}
	return
}

// The site owners map maps site names to owner email addresses.
type siteOwnerMap map[string]string

func loadSiteInfo(path string) (homedirMap, siteOwnerMap, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, nil, err
	}
	defer f.Close()
	var sites []*siteInfo
	if err := json.NewDecoder(f).Decode(&sites); err != nil {
		return nil, nil, err
	}

	homes := make(homedirMap)
	owners := make(siteOwnerMap)
	for _, s := range sites {
		homes[s.Docroot] = s.Site
		if s.isActive() {
			owners[s.Site] = s.Owner
		}
	}

	return homes, owners, nil
}
