// Package server implements the collection of detection events into a
// centralized database with an HTTP API.
//
// The difficulty in supporting both full and incremental scan modes
// is that we do not have a reliable signal for the disappearance of a
// detection. To maintain flexibility, the data model stores every raw
// detection entry and synthesizes path-level information from it, and
// we support an external method to explicitly acknowledge (resolve)
// entries.
//
package server

import (
	"database/sql"
	"errors"
	"log"
	"net/http"
	"strings"
	"time"

	"git.autistici.org/ai3/go-common/mail"
	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/go-common/sqlutil"
	"git.autistici.org/ai3/tools/yarascan"
)

// RawDetection is a detection event as stored in the database: a
// yarascan.Detection with additional site and state information.
type RawDetection struct {
	yarascan.Detection

	ID       int64
	Site     string
	Notified bool
	Resolved bool
}

var sqliteTimeFormat = "2006-01-02 15:04:05.999999999Z07:00"

var statements = map[string]string{

	"insert_detection": `
INSERT INTO detections (site, host, path, signature, timestamp) VALUES (?, ?, ?, ?, ?)
`,
	"find_raw_detections_by_site": `
SELECT
  id, site, host, path, signature, timestamp, resolved, notified
FROM
  detections
WHERE
  site = ?
`,
	"find_detections_by_site": `
SELECT
  site, host, path, GROUP_CONCAT(signature) AS signatures, MIN(timestamp) AS first_stamp, MAX(timestamp) AS last_stamp
FROM
  detections
WHERE
  site = ? AND resolved = 0
GROUP BY
  host, path
`,
	"find_unnotified_detections": `
SELECT
  site, host, path, GROUP_CONCAT(signature) AS signatures, MIN(timestamp) AS first_stamp, MAX(timestamp) AS last_stamp
FROM

  detections
WHERE
  resolved = 0 AND notified = 0
GROUP BY
  site, host, path
`,
	"latest_unacknowledged_detections": `
SELECT
  id, site, host, path, signature, timestamp, resolved, notified
FROM
  detections
WHERE
  resolved = 0
ORDER BY timestamp DESC LIMIT ?
`,
	"resolve_by_site": `
UPDATE detections SET resolved = 1 WHERE site = ?
`,
	"resolve_by_path": `
UPDATE detections SET resolved = 1 WHERE path = ? AND host = ?
`,
	"set_site_notified": `
UPDATE detections SET notified = 1 WHERE notified = 0 AND site = ? AND path = ? AND timestamp <= ?
`,
	"unresolved_paths_by_host": `
SELECT path FROM detections WHERE resolved = 0 AND host = ?
`,
}

// Config for a Server instance.
type Config struct {
	SiteInfoPath string       `yaml:"site_info_path"`
	NotifyAddr   string       `yaml:"notify_addr"`
	Mail         *mail.Config `yaml:"mail"`
}

// Server for the yarascan HTTP API.
type Server struct {
	homedirs   homedirMap
	siteOwners siteOwnerMap
	notifyAddr string
	mailer     *mail.Mailer

	db     *sql.DB
	doneCh chan bool
	stopCh chan bool
}

// New returns a new Server.
func New(database *sql.DB, config *Config) (*Server, error) {
	if config.SiteInfoPath == "" {
		return nil, errors.New("site_info_path not specified")
	}
	homedirs, siteOwners, err := loadSiteInfo(config.SiteInfoPath)
	if err != nil {
		return nil, err
	}

	var mailer *mail.Mailer
	if config.Mail != nil {
		mailer, err = mail.New(config.Mail)
		if err != nil {
			return nil, err
		}
	}

	s := &Server{
		homedirs:   homedirs,
		siteOwners: siteOwners,
		notifyAddr: config.NotifyAddr,
		mailer:     mailer,
		db:         database,
		stopCh:     make(chan bool),
		doneCh:     make(chan bool),
	}

	// Start the notification thread in the background.
	go func() {
		s.notifyLoop(s.stopCh)
		close(s.doneCh)
	}()

	return s, nil
}

// Close the Server and all associated resources.
func (s *Server) Close() {
	close(s.stopCh)
	<-s.doneCh
}

// Post-process the incoming Detection objects before they are saved
// to the database. Use this function to add whatever metadata
// augmentation logic we desire.
func (s *Server) processDetection(d *yarascan.Detection) *RawDetection {
	rd := RawDetection{Detection: *d}
	if site, ok := s.homedirs.Find(d.Path); ok {
		rd.Site = site
	}
	return &rd
}

func (s *Server) insertDetections(tx *sql.Tx, dd []*yarascan.Detection) error {
	stmt, err := tx.Prepare(statements["insert_detection"])
	if err != nil {
		return err
	}
	defer stmt.Close()

	for _, d := range dd {
		rd := s.processDetection(d)
		_, err := stmt.Exec(
			rd.Site,
			rd.Host,
			rd.Path,
			rd.Signature,
			rd.Timestamp,
		)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Server) findDetections(tx *sql.Tx, queryName string, args ...interface{}) ([]*yarascan.PathDetection, error) {
	rows, err := tx.Query(statements[queryName], args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var byPath []*yarascan.PathDetection
	for rows.Next() {
		var site, host, path, sigs string
		// For whatever reason sqlite returns these fields as
		// strings instead of datetime objects.
		var firstStamp, lastStamp string
		if err := rows.Scan(&site, &host, &path, &sigs, &firstStamp, &lastStamp); err != nil {
			return nil, err
		}
		pd := &yarascan.PathDetection{
			Site:       site,
			Host:       host,
			Path:       path,
			Signatures: strings.Split(sigs, ","),
		}
		if t, err := time.Parse(sqliteTimeFormat, firstStamp); err == nil {
			pd.FirstStamp = t
		}
		if t, err := time.Parse(sqliteTimeFormat, lastStamp); err == nil {
			pd.LastStamp = t
		}
		byPath = append(byPath, pd)
	}
	return byPath, nil
}

func (s *Server) findUnresolvedDetectionPaths(tx *sql.Tx, host string) ([]string, error) {
	rows, err := tx.Query(statements["unresolved_paths_by_host"], host)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var out []string
	for rows.Next() {
		var path string
		if err := rows.Scan(&path); err != nil {
			return nil, err
		}
		out = append(out, path)
	}
	return out, nil
}

func (s *Server) findUnnotifiedDetections(tx *sql.Tx) ([]*yarascan.PathDetection, error) {
	return s.findDetections(tx, "find_unnotified_detections")
}

func (s *Server) resolve(tx *sql.Tx, host string, entries []*yarascan.ResolveEntry) error {
	for _, entry := range entries {
		var err error
		switch {
		case entry.Site != "":
			_, err = tx.Exec(statements["resolve_by_site"], entry.Site)
		case entry.Path != "":
			_, err = tx.Exec(statements["resolve_by_path"], entry.Path, host)
		default:
			err = errors.New("empty resolution criteria")
		}
		if err != nil {
			return err
		}
	}
	return nil
}

// HTTP handlers

func (s *Server) handleSubmission(w http.ResponseWriter, r *http.Request) {
	var req yarascan.SubmissionRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	if err := sqlutil.WithTx(r.Context(), s.db, func(tx *sql.Tx) error {
		return s.insertDetections(tx, req.Detections)
	}); err != nil {
		log.Printf("submission error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, nil)
}

func (s *Server) handleFindDetectionsBySite(w http.ResponseWriter, r *http.Request) {
	var req yarascan.FindDetectionsBySiteRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	var response yarascan.FindDetectionsBySiteResponse

	if err := sqlutil.WithReadonlyTx(r.Context(), s.db, func(tx *sql.Tx) (err error) {
		response.Detections, err = s.findDetections(tx, "find_detections_by_site", req.Site)
		return
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, &response)
}

func (s *Server) handleUnresolvedDetectionPaths(w http.ResponseWriter, r *http.Request) {
	var req yarascan.FindUnresolvedDetectionPathsRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	var response yarascan.FindUnresolvedDetectionPathsResponse

	if err := sqlutil.WithReadonlyTx(r.Context(), s.db, func(tx *sql.Tx) (err error) {
		response.Paths, err = s.findUnresolvedDetectionPaths(tx, req.Host)
		return
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, &response)
}

func (s *Server) handleResolve(w http.ResponseWriter, r *http.Request) {
	var req yarascan.ResolveRequest
	if !serverutil.DecodeJSONRequest(w, r, &req) {
		return
	}

	if err := sqlutil.WithTx(r.Context(), s.db, func(tx *sql.Tx) error {
		return s.resolve(tx, req.Host, req.Entries)
	}); err != nil {
		log.Printf("resolve error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serverutil.EncodeJSONResponse(w, nil)
}

// Handler returns a http.Handler for the server API. All URL paths
// start with /api/.
func (s *Server) Handler() http.Handler {
	h := http.NewServeMux()
	h.HandleFunc("/api/search/by_site", s.handleFindDetectionsBySite)
	h.HandleFunc("/api/search/unresolved", s.handleUnresolvedDetectionPaths)
	h.HandleFunc("/api/submission", s.handleSubmission)
	h.HandleFunc("/api/resolve", s.handleResolve)
	return h
}
