package ui

import (
	"fmt"
	"html/template"
)

var sriMap = map[string]string{
	"/static/css/bootstrap.min.css": "sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T",
	"/static/css/open-iconic-bootstrap.min.css": "sha384-wWci3BOzr88l+HNsAtr3+e5bk9qh5KfjU6gl/rbzfTYdsAVHBEbxB33veLYmFg/a",
	"/static/js/bootstrap.bundle.min.js": "sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o",
	"/static/js/jquery.slim.min.js": "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo",
}

// SRIScript returns a <script> tag with resource integrity attributes.
func SRIScript(uri string) template.HTML {
	s := fmt.Sprintf("<script src=\"%s\"", uri)
	if sri, ok := sriMap[uri]; ok {
		s += fmt.Sprintf(" crossorigin=\"\" integrity=\"%s\"", sri)
	}
	s += "></script>"
	return template.HTML(s) // nolint: gosec
}

// SRIStylesheet returns a <link> tag with resource integrity attributes.
func SRIStylesheet(uri string) template.HTML {
	s := fmt.Sprintf("<link rel=\"stylesheet\" type=\"text/css\" href=\"%s\"", uri)
	if sri, ok := sriMap[uri]; ok {
		s += fmt.Sprintf(" integrity=\"%s\"", sri)
	}
	s += ">"
	return template.HTML(s) // nolint: gosec
}

