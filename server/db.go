package server

import (
	"database/sql"

	"git.autistici.org/ai3/go-common/sqlutil"
)

var migrations = []func(*sql.Tx) error{
	sqlutil.Statement(`
CREATE TABLE detections (
       id INTEGER PRIMARY KEY,
       site TEXT,
       host TEXT,
       path TEXT,
       signature TEXT,
       timestamp DATETIME,
       notified BOOL NOT NULL DEFAULT 0,
       resolved BOOL NOT NULL DEFAULT 0
);
`, `
CREATE INDEX idx_detections_site ON detections (site);
`, `
CREATE INDEX idx_detections_host_path ON detections (host, path);
`, `
CREATE INDEX idx_detections_signature ON detections (signature);
`),
}

func OpenDB(dburi string) (*sql.DB, error) {
	return sqlutil.OpenDB(dburi, sqlutil.WithMigrations(migrations))
}
