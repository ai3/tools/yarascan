package pgp

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"net/mail"
	"net/textproto"
	"os"
	"strconv"

	"golang.org/x/crypto/openpgp"

	"git.autistici.org/ai3/go-common/mail/message"
)

// Signer is a message.Middleware implementation that can
// transparently sign MIME messages and turn them into PGP/MIME
// messages.
type Signer struct {
	sender     *mail.Address
	signKey    *openpgp.Entity
	encodedKey string
}

// NewSigner creates a new Signer. The key ID can be the empty string,
// in which case the first usable key found in the keyring will be
// automatically selected. If specified, the key ID must be in
// long-form hex format.
func NewSigner(keyFile, keyID string, sender *mail.Address) (*Signer, error) {
	signKey, err := loadPGPKey(keyFile, keyID)
	if err != nil {
		return nil, err
	}

	var buf bytes.Buffer
	basew := base64.NewEncoder(base64.StdEncoding, &buf)
	if err := signKey.PrimaryKey.Serialize(basew); err != nil {
		return nil, err
	}

	return &Signer{
		sender:     sender,
		signKey:    signKey,
		encodedKey: buf.String(),
	}, nil
}

func loadPGPKey(path, keyID string) (*openpgp.Entity, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	keyring, err := openpgp.ReadKeyRing(f)
	if err != nil {
		f.Seek(0, 0) // nolint
		keyring, err = openpgp.ReadArmoredKeyRing(f)
		if err != nil {
			return nil, err
		}
	}

	// The key ID must be converted to uint64.
	uKeyID, err := strconv.ParseUint(keyID, 16, 64)
	if err != nil {
		return nil, fmt.Errorf("error parsing key ID: %v", err)
	}

	var key *openpgp.Key
	if keyID != "" {
		if keys := keyring.KeysById(uKeyID); len(keys) > 0 {
			key = &keys[0]
		}
	} else {
		if keys := keyring.DecryptionKeys(); len(keys) > 0 {
			key = &keys[0]
		}
	}
	if key == nil {
		return nil, errors.New("unable to find key in keyring")
	}

	return key.Entity, nil
}

func (s *Signer) pgpSign(data []byte) ([]byte, error) {
	var buf bytes.Buffer
	if err := openpgp.ArmoredDetachSign(&buf, s.signKey, bytes.NewReader(data), nil); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// Split headers that go inside the signed message and those that
// should stay outside (on the wrapped message).
func (s *Signer) pgpSplitHeaders(hdr textproto.MIMEHeader) (textproto.MIMEHeader, textproto.MIMEHeader) {
	inner := make(textproto.MIMEHeader)
	outer := make(textproto.MIMEHeader)
	outer.Set("Openpgp", "preference=signencrypt")
	outer.Set("Autocrypt", fmt.Sprintf("addr=%s; keydata=%s", s.sender.Address, s.encodedKey))

	for k, vv := range hdr {
		switch k {
		case "Content-Type", "Content-Transfer-Encoding", "Content-Disposition", "Content-Description":
			inner[k] = vv
		case "From", "To", "Subject", "Message-Id":
			inner[k] = vv
			outer[k] = vv
		default:
			outer[k] = vv
		}
	}
	return inner, outer
}

// Process a message.Part, signing it with our PGP key and creating a
// PGP/MIME message. Implements the message.Middleware interface.
func (s *Signer) Process(p *message.Part) (*message.Part, error) {
	// Split the headers and apply PGP headers on the container.
	// Modify the Part before signing it!
	innerHdr, outerHdr := s.pgpSplitHeaders(p.Header)
	p.Header = innerHdr

	// We need to serialize the message in order to sign it.
	var buf bytes.Buffer
	if err := p.Render(&buf); err != nil {
		return nil, err
	}

	signature, err := s.pgpSign(buf.Bytes())
	if err != nil {
		return nil, err
	}

	wrap := message.NewMultiPart(
		"multipart/signed; micalg=pgp-sha256; protocol=\"application/pgp-signature\"",
		p,
		&message.Part{
			Header: textproto.MIMEHeader{
				"Content-Type":        []string{"application/pgp-signature; name=\"signature.asc\""},
				"Content-Description": []string{"OpenPGP digital signature"},
				"Content-Disposition": []string{"attachment; filename=\"signature.asc\""},
			},
			Body: signature,
		},
	)
	for k, vv := range outerHdr {
		wrap.Header[k] = vv
	}
	return wrap, nil
}
